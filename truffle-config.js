var HDWalletProvider = require("truffle-hdwallet-provider");

var mnemonic = "610C42C9723E95CB2F61D151BCF325B4EC9E147AB105174CC97FCDC7A1EB47BA";

module.exports = {
    networks:
        {
            development: {
                host: "127.0.0.1",
                port: 7545,
                network_id: "5777" // Match any network id
            },
            ropsten: {
                provider:
                new HDWalletProvider(mnemonic, "https://ropsten.infura.io/"),
                network_id: '3',
            },
            main: {
                provider:
                new HDWalletProvider(mnemonic, "https://mainnet.infura.io/v3/dcd37fe9b8634ce4bca75ef9e5d0fadb"),
                network_id: '1',
            }
        }
};

